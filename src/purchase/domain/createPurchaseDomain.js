const uuid = require('uuid');

const { createPurchaseInputSchema } = require('../schema/input/createPurchaseInput');
const { createPurchaseService } = require('../service/createPurchaseService');

const { getClientService } = require('../../client/service/getClientService');
// const { publishCreatePurchase } = require('../service/publishCreatePurchase');
// const { createPurchaseEvent } = require('../schema/event/createPurchaseEvent');

// const { calculateAge } = require("../helper/createPurchase");
const { calculatePrice } = require("../helper/purchase");

const createPurchaseDomain = async (commandPayload, commandMeta) => {

  commandPayload.forEach(purchase => {

    purchase.id = uuid.v4();

    new createPurchaseInputSchema(purchase);

    const client = await getClientService(purchase);

    if(client) {

      await createPurchaseService(calculatePrice(client, purchase));


    }

    
  });




    // if (
    //     calculateAge(commandPayload.birth) < 18 ||
    //     calculateAge(commandPayload.birth) > 65
    // ) {
    //     return {
    //       status: 400,
    //       body: "Purchase must be between 18 and 65 years old",
    //     };
    // }


    // if(client) {
    //   await createPurchaseService(calculatePrice(commandPayload, client));
    //   return {
    //     status: 200,
    //     body: "Purchase added succesfully",
    //   };
    // }
    
    // return {
    //   status: 404,
    //   body: "User not found",
    // };
    
    // await publishCreatePurchase(new createPurchaseEvent(commandPayload, commandMeta));
    
    
};

module.exports = { createPurchaseDomain };