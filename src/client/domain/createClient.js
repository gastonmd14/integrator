const { createClientInputSchema } = require('../schema/input/createClient');
const { saveClient } = require('../service/saveClient');

const createClientDomain = async (commandPayload, commandMeta) => {
    const client = new createClientInputSchema(commandPayload, commandMeta).get();
    await saveClient({ pk: 'client', sk: client.dni, ...client });
    return { status: 200, body: client };
};

module.exports = { createClientDomain };