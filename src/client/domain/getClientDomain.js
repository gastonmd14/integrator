const { getClientInputSchema } = require('../schema/input/getClientInput');
const { getClientService } = require('../service/getClientService');

const getClientDomain = async (commandPayload, commandMeta) => {
    new getClientInputSchema(commandPayload, commandMeta);

    const client = await getClientService(commandPayload);
    
    return {
        status: 200,
        body: client,
    };
};

module.exports = { getClientDomain };