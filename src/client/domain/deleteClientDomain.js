const { deleteClientInputSchema } = require('../schema/input/deleteClientInput');
const { deleteClientService } = require('../service/deleteClientService');

const timestamp = new Date().toISOString();

const deleteClientDomain = async (commandPayload, commandMeta) => {
    new deleteClientInputSchema(commandPayload, commandMeta);

    commandPayload.deleted_at = timestamp;
    await deleteClientService(commandPayload);
    
    return {
        status: 204,
        body: "Client removed succesfully",
    };
};

module.exports = { deleteClientDomain };