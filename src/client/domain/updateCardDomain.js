const { updateCardService } = require('../service/updateCardService');

const { randomNumber} = require('../helper/createClient');
const { calculateAge } = require("../helper/createClient");

const updateCardDomain = async (eventPayload, eventMeta) => {

  const event = JSON.parse(eventPayload.Message);

  const creditCardNumber = `${randomNumber(0000,9999)}-${randomNumber(0000,9999)}-${randomNumber(0000,9999)}-${randomNumber(0000,9999)}`
  const expirationDate = `${randomNumber(01,12)}/${randomNumber(21,35)}`
  const securityCode = `${randomNumber(000,999)}`

  let type = calculateAge(event.birth) > 45 ? 'Gold' : 'Classic'

  const params = {
    TableName: process.env.CLIENT_TABLE,
    Key: { 'dni' : event.dni },
    UpdateExpression: "SET #C = :c",
    ExpressionAttributeNames: { "#C": "creditCard" },
    ExpressionAttributeValues: {
      ":c": {
        M: {
          "number": { S: creditCardNumber },
          "expiration": { S: expirationDate },
          "ccv": { S: securityCode },
          "type":{ S: type }
        },
      },
    },
    ReturnValues: "ALL_NEW",
  };

  await updateCardService(params);

  return {
    status: 200,
    body: "Card added succesfully",
  };
};

module.exports = { updateCardDomain };