const { createGiftInputSchema } = require('../schema/input/createGiftInput');
const { createGiftService } = require('../service/createGiftService');

const { giftChoser } = require('../helper/createClient');

const createGiftDomain = async (eventPayload, eventMeta) => {
  const event = JSON.parse(eventPayload.Message);

  let whatGift = giftChoser(event.birth);

  const params = {
    TableName: process.env.CLIENT_TABLE,
    Key: { "dni": event.dni },
    UpdateExpression: "SET #G = :g",
    ExpressionAttributeNames: { "#G": "gift" },
    ExpressionAttributeValues: { ":g": whatGift },
    ReturnValues: "ALL_NEW",
  }; 
  
  await createGiftService(params);  
    
  return {
    status: 200,
    body: "Gift added succesfully",
  };
};

module.exports = { createGiftDomain };