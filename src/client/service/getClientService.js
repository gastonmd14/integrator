const dynamo = require('ebased/service/storage/dynamo');

async function getClientService(commandPayload) {
    const { Item } = await dynamo.getItem({
    TableName: process.env.CLIENT_TABLE,
    Key: { 'dni': commandPayload.dni },
  });
  if (!Item) throw new ErrorHandled('Missing Client', { status: 404, code: 'NOT_FOUND' });
  if(Item.deleted_at === '') {
    Object.keys(Item).forEach(k => { if (k === 'dni') delete Item[k] });
    return Item;
  }
}
  

module.exports = { getClientService };