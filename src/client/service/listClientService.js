const dynamo = require('ebased/service/storage/dynamo');

async function listClientService() {
  const { Items } = await dynamo.scanTable({
    TableName: process.env.CLIENT_TABLE,
  });
  return Items.map(e => {
    if(e.deleted_at === ''){
      Object.keys(e).forEach(k => {
        if (k === 'dni') delete e[k] 
      });
      return e;
    }
  });
}

module.exports = { listClientService };