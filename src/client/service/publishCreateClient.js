const sns = require("ebased/service/downstream/sns");

async function publishCreateClient(clientCreatedEvent) {

  const { eventPayload, eventMeta } = clientCreatedEvent.get();

  const snsPublishParams = {
    TopicArn: process.env.CLIENT_TOPIC,
    Message: eventPayload,
  };
  
  await sns.publish(snsPublishParams, eventMeta);
}

module.exports = { publishCreateClient };
