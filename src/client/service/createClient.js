const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const TABLE_NAME = config.get('CLIENT_TABLE');

const saveClient = async (client) => dynamo.putItem({ TableName: TABLE_NAME, Client: client });

module.exports = { saveClient };