const dynamo = require('ebased/service/storage/dynamo');

async function deleteClientService(commandPayload) {
    
    const params = {
      TableName: process.env.CLIENT_TABLE,
      Key: { 'dni': commandPayload.dni },
      UpdateExpression: 'SET',
      ExpressionAttributeNames: {},
      ExpressionAttributeValues: {},
      ReturnValues: 'ALL_NEW',
    }
    
    Object.keys(commandPayload).forEach(key => {
      if(key !== 'dni') {
        params.UpdateExpression += ` #${key}=:${key},`;
        params.ExpressionAttributeNames[`#${key}`] = key;
        params.ExpressionAttributeValues[`:${key}`] = commandPayload[key];
      }
    });
  
    params.UpdateExpression = params.UpdateExpression.slice(0, -1);
  
    const { Attributes } = await dynamo.updateItem(params);
  
    Object.keys(Attributes).forEach(k => { if (k === 'dni') delete Attributes[k] });
    return Attributes;

  
}

module.exports = { deleteClientService };