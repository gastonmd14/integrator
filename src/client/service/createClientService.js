const dynamo = require('ebased/service/storage/dynamo');

const timestamp = new Date().toISOString();

async function createClientService(commandPayload) {
  await dynamo.putItem({
    TableName: process.env.CLIENT_TABLE,
    Item: {
      'created_at': timestamp,
      'updated_at': '',
      'deleted_at': '',
      ...commandPayload
    }
  });
}

module.exports = { createClientService };