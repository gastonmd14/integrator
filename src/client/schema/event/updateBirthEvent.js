const { DownstreamEvent } = require('ebased/schema/downstreamEvent');

class updateBirthEvent extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      type: 'UPDATE_BIRTH',
      specversion: 'v1.0.0',
      payload: payload,
      meta: meta,
      schema: {
        strict: false,
        dni: { type: String, required: true },
        birth: { type: String, required: true }
      },
    })
  }
}

module.exports = { updateBirthEvent };