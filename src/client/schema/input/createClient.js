const { InputValidation } = require('ebased/schema/inputValidation');

class createClientInputSchema extends InputValidation {
    constructor(payload, meta) {
      super({
        source: meta.status,
        payload: payload,
        source: 'FOOD.CREATE',
        specversion: 'v1.0.0',
        schema: {
          strict: false,
          id: { type: String, required: true },
          name: { type: String, required: true },
        },
      });
    }
};

module.exports = { createClientInputSchema };